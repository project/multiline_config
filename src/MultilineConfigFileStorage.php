<?php

namespace Drupal\multiline_config;

use Drupal\Core\Config\FileStorage;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;
use function is_string;

/**
 * Defines the multiline config file storage.
 */
class MultilineConfigFileStorage extends FileStorage {

  /**
   * {@inheritdoc}
   *
   * @see https://www.drupal.org/project/drupal/issues/2844452
   */
  public function encode($data) {
    // \Symfony\Component\Yaml\Dumper::dump doesn't split lines with CRLF, so
    // we need to replace CRLF by LF and remove extra returns after arrays
    // delimiter.
    array_walk_recursive(
      $data,
      function (&$value) {
        if (is_string($value) && strpos($value, PHP_EOL) !== FALSE) {
          $value = preg_replace(["/\r\n/", "/\n+$/"], [PHP_EOL, ''], $value);
        }
      }
    );

    // Set the indentation to 2 to match Drupal's coding standards.
    $dumper = new Dumper(2);
    $yaml = $dumper->dump($data, PHP_INT_MAX, 0, SymfonyYaml::DUMP_EXCEPTION_ON_INVALID_TYPE | SymfonyYaml::DUMP_MULTI_LINE_LITERAL_BLOCK);

    // Manually rewrite our blocks to use the 'strip' chomping indicator. See
    // https://yaml-multiline.info for more information. This prevents trailing
    // newline characters that can cause issues with some contrib modules that
    // iterate over configuration by newline character. There's no way to tell
    // \Symfony\Component\Yaml\Dumper to do this, unfortunately.
    $yaml = str_replace(": |\n", ": |-\n", $yaml);

    // Make sure the last line has a blank line.
    if (substr($yaml, -1) !== "\n") {
      $yaml .= "\n";
    }

    return $yaml;
  }

}
